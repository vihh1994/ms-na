package br.com.autbank.seguranca.jwt

import lombok.AllArgsConstructor
import lombok.Getter
import lombok.Setter
import lombok.ToString

@Getter
@Setter
@AllArgsConstructor
@ToString
class CredencialAcesso {
    private val usuario: String = ""
    private val senha: String = ""

}