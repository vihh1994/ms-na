package br.com.autbank.excecoes

import lombok.NoArgsConstructor
import java.io.PrintWriter
import java.io.StringWriter

@NoArgsConstructor
class ManipulaExcecoes {

    fun getStackTraceText(e: Throwable): String {
        val sw = StringWriter()
        val pw = PrintWriter(sw)
        e.printStackTrace(pw)
        return sw.toString()
    }
}