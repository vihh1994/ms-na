import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.asciidoctor.convert") version "1.5.3"
	kotlin("plugin.jpa") version "1.2.71"
	id("org.springframework.boot") version "2.1.7.RELEASE"
	id("io.spring.dependency-management") version "1.0.7.RELEASE"
	kotlin("jvm") version "1.2.71"
	kotlin("plugin.spring") version "1.2.71"
}

java.sourceCompatibility = JavaVersion.VERSION_1_8

ext {
	set("snippetsDir", file("build/generated-snippets"))
}

allprojects {
	group = "br.com.autbank"
	version = "0.0.1-SNAPSHOT"

	apply{
		plugin("java")
		plugin("io.spring.dependency-management")
		plugin("kotlin-spring")
		plugin("application")
	}

//	application {
//		mainClassName = "br.com.autbank."
//	}

	val developmentOnly by configurations.creating
	configurations {
		runtimeClasspath {
			extendsFrom(developmentOnly)
		}
		compileOnly {
			extendsFrom(configurations.annotationProcessor.get())
		}
	}

	repositories {
		mavenCentral()
	}

	dependencies {
		implementation("org.springframework.boot:spring-boot-starter-actuator")
		implementation("org.springframework.boot:spring-boot-starter-data-jpa")
		implementation("org.springframework.boot:spring-boot-starter-web")
		implementation("org.springframework.boot:spring-boot-starter-web-services")
		implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
		implementation("org.jetbrains.kotlin:kotlin-reflect")
		implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
		compileOnly("org.projectlombok:lombok")
		developmentOnly("org.springframework.boot:spring-boot-devtools")
		implementation("org.projectlombok:lombok")
		testImplementation("org.springframework.boot:spring-boot-starter-test")
		testImplementation("org.springframework.restdocs:spring-restdocs-mockmvc")

		// DB
		implementation("com.microsoft.sqlserver:mssql-jdbc")
	}
}

dependencies {
	compile(project(":Configuracao"))
	compile(project(":Descriptografia"))
	compile(project(":Seguranca"))
	compile(project(":Utilidades"))
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}

tasks.test {
	outputs.dir(ext.get("snippetsDir")!!)
}

tasks.asciidoctor {
	inputs.dir(ext.get("snippetsDir")!!)
	dependsOn(tasks.test)
}
