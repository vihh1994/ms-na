package br.com.autbank.NAMS

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["br.com.autbank"])
class NaMsApplication

fun main(args: Array<String>) {
	runApplication<NaMsApplication>(*args)
}
