package br.com.autbank;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class DocumentoXMLDOM {
    private final static int NAOFORMATA = 0;
    private final static int FORMATAECOM = 1;
    private final static int FORMATAECOMMENORMAIOR = 2;
    private Document xmldom;

    public DocumentoXMLDOM(String file, int a) throws Exception {
        try {
            SAXBuilder builder = new SAXBuilder();
            xmldom = builder.build(file);
        } catch (JDOMException e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public DocumentoXMLDOM(String xmlin) throws Exception {
        this(xmlin, false);
    }

    public DocumentoXMLDOM(String xmlin, boolean verifica) throws Exception {
        if ((xmlin == null) || xmlin.trim().equals(""))
            throw new Exception("XML nulo ou branco.");
        String msg = "";

        // long inicio = System.currentTimeMillis();
        SAXBuilder builder = new SAXBuilder();
        xmlin = formata(xmlin);
        try {
            xmldom = builder.build(new StringReader(xmlin));
        } catch (JDOMException e) {

            // e.printStackTrace();
            throw new Exception("Xml inv�lido. " + msg + ". " + e.getMessage() + " "
                    + ((xmlin.length() > 1000) ? (xmlin.substring(0, 1000) + " ...") : xmlin));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            throw new Exception("Xml inv�lido. " + msg + ". " + e.getClass().toString() + " " + e.getMessage() + " "
                    + ((xmlin.length() > 1000) ? (xmlin.substring(0, 1000) + " ...") : xmlin));
        } finally {

            // System.out.println ("XML: " +
            // Long.toString(System.currentTimeMillis()-inicio) + " ms.");
        }
    }

    private String formata(String string) {
        String str = formata(string, FORMATAECOM);
        return str;
    }

    private static String formata(String valor, int tipo) {
        if ((valor == null) || (tipo == NAOFORMATA))
            return valor;
        StringBuffer rs = new StringBuffer("");
        for (int i = 0; i < valor.length(); i++) {
            if (valor.charAt(i) == '&') {
                String frag = valor.substring(i, i + 6);
                if (frag.equals("&apos;")) {
                    rs.append("&apos;");
                    i += 5;
                    continue;
                } else if (frag.equals("&quot;")) {
                    rs.append("&quot;");
                    i += 5;
                    continue;
                }
                frag = frag.substring(0, 5);
                if (frag.equals("&amp;")) {
                    rs.append("&amp;");
                    i += 4;
                    continue;
                }
                frag = frag.substring(0, 4);
                if (frag.equals("&gt;")) {
                    rs.append("&gt;");
                    i += 3;
                    continue;
                } else if (frag.equals("&lt;")) {
                    rs.append("&lt;");
                    i += 3;
                    continue;
                }
                rs.append("&amp;");

            } else if ((valor.charAt(i) == '>') && (tipo == FORMATAECOMMENORMAIOR)) {
                rs.append("&gt;");
            } else if ((valor.charAt(i) == '<') && (tipo == FORMATAECOMMENORMAIOR)) {
                rs.append("&lt;");
            } else {
                rs.append(valor.charAt(i));
            }
        }
        return rs.toString();
    }

    public String toString() {
        return formata(buscaXML(xmldom.getRootElement()));
    }

    public String selectSingleNode(String tag) {
        return buscaTag(tag);
    }

    public String selectDocumentElement() {

        // return this.xmldom.detachRootElement().getText();
        return selectDocumentElement(getRootName());
    }

    public String getRootName() {
        return xmldom.getRootElement().getName();
    }

    public String selectDocumentElement(String tag) {
        return this.buscaXML(tag, false);
    }

    public DocumentoXMLDOM[] selectNodes(String tag) throws Exception {
        Vector v = new Vector();
        DocumentoXMLDOM.buscaNodes(xmldom.getRootElement(), tag, v);
        DocumentoXMLDOM[] r = new DocumentoXMLDOM[v.size()];
        for (int i = 0; i < v.size(); i++) {
            Object o = v.get(i);
            r[i] = new DocumentoXMLDOM(DocumentoXMLDOM.buscaXML((Element) o));
        }
        return r;
    }

    private static void buscaNodes(Element element, String tag, Vector v) {
        if (element == null)
            return;
        List content = element.getChildren();
        Iterator iterator = content.iterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            if (o instanceof Element) {
                if (tag.equals(((Element) o).getName()))
                    v.add(o);
                DocumentoXMLDOM.buscaNodes((Element) o, tag, v);
            }
        }
    }

    public DocumentoXMLDOM[] selectNodesStartsWith(String tag) throws Exception {
        Vector v = new Vector();
        DocumentoXMLDOM.buscaNodesComecamCom(xmldom.getRootElement(), tag, v);
        DocumentoXMLDOM[] r = new DocumentoXMLDOM[v.size()];
        for (int i = 0; i < v.size(); i++) {
            Object o = v.get(i);
            r[i] = new DocumentoXMLDOM(DocumentoXMLDOM.buscaXML((Element) o));
        }
        return r;
    }

    private static void buscaNodesComecamCom(Element element, String tag, Vector v) {
        if (element == null)
            return;
        List content = element.getChildren();
        Iterator iterator = content.iterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            if (o instanceof Element) {
                if (((Element) o).getName().startsWith(tag))
                    v.add(o);
                DocumentoXMLDOM.buscaNodesComecamCom((Element) o, tag, v);
            }
        }
    }

    public String buscaTag(String tag) {
        return buscaTag(tag, false);
    }

    public String buscaTag(String tag, boolean tratado) {
        String r = DocumentoXMLDOM.buscaTag(xmldom.getRootElement(), tag, tratado);
        return (r == null) ? "" : r;
    }

    private static String buscaTag(Element element, String tag, boolean tratado) {
        if (element == null)
            return null;
        if (tag.equals(element.getName()))
            return tratado ? formata(element.getText(), FORMATAECOMMENORMAIOR) : element.getText();

        List content = element.getChildren();
        Iterator iterator = content.iterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            if (o instanceof Element) {
                String r = DocumentoXMLDOM.buscaTag((Element) o, tag, tratado);
                if (r != null)
                    return r;
            }
        }
        return null;
    }

    private static String buscaElementoAtributo(Element element, String tag, String atributo) {
        if (element == null)
            return null;
        if (element.getName().equals(tag)) {
            List attrs = element.getAttributes();
            Iterator iterator = attrs.iterator();
            while (iterator.hasNext()) {
                Object o = iterator.next();
                if (o instanceof Attribute) {
                    Attribute a = (Attribute) o;
                    if (a.getName().equals(atributo))
                        return a.getValue();
                }
            }
            return null;
        }
        List content = element.getChildren();
        Iterator iterator = content.iterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            if (o instanceof Element) {
                return DocumentoXMLDOM.buscaElementoAtributo((Element) o, tag, atributo);
            }
        }
        return null;
    }

    public String buscaElementoAtributo(String tag, String atributo) {
        String v = buscaElementoAtributo(xmldom.getRootElement(), tag, atributo);
        return (v == null) ? "" : v;
    }

    public String buscaXML(String tag) {
        return buscaXML(tag, true);
    }

    private String buscaXML(String tag, boolean comRaiz) {
        String r = DocumentoXMLDOM.buscaXML(xmldom.getRootElement(), tag, comRaiz);
        return (r == null) ? "" : r;
    }

    private static String buscaXML(Element element, String tag, boolean comRaiz) {
        if (element == null)
            return null;
        if (tag.equals(element.getName()))
            return DocumentoXMLDOM.buscaXML(element, comRaiz);
        List content = element.getChildren();
        Iterator iterator = content.iterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            if (o instanceof Element) {
                String r = DocumentoXMLDOM.buscaXML((Element) o, tag, comRaiz);
                if (r != null)
                    return r;
            }
        }
        return null;
    }

    private static String buscaXML(Element element) {
        return DocumentoXMLDOM.buscaXML(element, true);
    }

    private static String buscaXML(Element element, boolean comRaiz) {
        if (element == null)
            return null;
        StringBuffer sb = new StringBuffer();
        if (comRaiz) {
            sb.append("<");
            sb.append(element.getName());
            List attrs = element.getAttributes();
            Iterator iterator = attrs.iterator();
            while (iterator.hasNext()) {
                Object o = iterator.next();
                if (o instanceof Attribute) {
                    Attribute a = (Attribute) o;
                    sb.append(" ");
                    sb.append(a.getName());
                    sb.append("=\"");
                    sb.append(a.getValue());
                    sb.append("\"");
                }
            }
            sb.append(">");
        }
        sb.append(formata(element.getText(), FORMATAECOMMENORMAIOR));

        List content = element.getChildren();
        Iterator iterator = content.iterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            if (o instanceof Element) {
                sb.append(DocumentoXMLDOM.buscaXML((Element) o));
            }
        }
        if (comRaiz) {
            sb.append("</");
            sb.append(element.getName());
            sb.append(">");
        }
        return sb.toString();
    }
}
