package br.com.autbank

import sun.misc.BASE64Decoder
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

class Descriptografar {
    @Throws(Exception::class)
    fun consArqSeg(caminho: String): String {
        var linha = ""
        val file = File(caminho)
        if (!file.exists()) {
            throw Exception("Arquivo de seguranca nao encontrado. " + file.absolutePath)
        } else {
            val reader = FileReader(file)
            val leitor = BufferedReader(reader)
            linha = leitor.readLine()
            leitor.close()
            reader.close()
        }
        return linha
    }

    @Throws(Exception::class)
    fun consUsuario(xml: String, sistema: String): String {
        try {
            val xmlTodos = DocumentoXMLDOM(xml)
            val xmlSistema = xmlTodos.buscaXML(sistema)
            val xmlParcial = DocumentoXMLDOM(xmlSistema)
            return xmlParcial.buscaTag("USUARIO")
        } catch (e: Exception) {
            throw Exception(e.message)
        }

    }

    @Throws(Exception::class)
    fun consSenha(xml: String, sistema: String): String {
        try {
            val xmlTodos = DocumentoXMLDOM(xml)
            val xmlSenha = xmlTodos.buscaXML(sistema)
            val xmlParcial = DocumentoXMLDOM(xmlSenha)
            return xmlParcial.buscaTag("SENHA")
        } catch (e: Exception) {
            throw Exception(e.message)
        }

    }

    @Throws(Exception::class)
    fun decodificaTexto(textoHex: String): String {
        try {
            val chave = byteArrayOf(-12, -125, -79, -48, 112, 102, 115, 100, 52, 51, 50, 49, -92, 36, 58, -58)//key.getBytes();
            val skeySpecChave = SecretKeySpec(chave, "AES")
            val algoritmo = Cipher.getInstance("AES/ECB/NoPadding")
            algoritmo.init(Cipher.DECRYPT_MODE, skeySpecChave)
            val dec = BASE64Decoder()
            val texto = dec.decodeBuffer(textoHex)
            val textoDecodificado = algoritmo.doFinal(texto)
            return String(textoDecodificado)
        } catch (e: Exception) {
            throw Exception(e)
        }

    }

    private fun completaBytes(texto: ByteArray): ByteArray {
        val tamTexto = texto.size
        val divididoPor16 = tamTexto % 16
        var completa16 = 0
        if (divididoPor16 != 0) {
            completa16 = 16 - divididoPor16
        }
        val total = tamTexto + completa16
        val novoTexto = ByteArray(total)
        for (i in 0 until tamTexto) {
            novoTexto[i] = texto[i]
        }
        if (divididoPor16 != 0) {
            for (i in tamTexto until total) {
                novoTexto[i] = 0x00
            }
        }
        return novoTexto
    }
}